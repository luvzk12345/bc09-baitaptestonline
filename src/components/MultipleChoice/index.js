//17
import React, { Component } from "react";
import { actionType } from "../../store/actions/type";
import { createAction } from "../../store/actions/index";
import { connect } from "react-redux";

class MultipleChoice extends Component {
  //18
  constructor(props) {
    super(props);
    this.state = {
      formValue: {
        questionId: "",
        answer: {
          content: "",
          exact: "",
        },
      },
    };
  }

  handleChange = async (event) => {
    await this.setState({
      formValue: {
        questionId: this.props.item.id,
        answer: {
          content: event.target.title,
          exact: this.props.answer.exact,
        },
      },
    });
    console.log(this.state.formValue);

    this.props.dispatch(
      createAction(actionType.SET_ANSWER, this.state.formValue)
    );
  };

  render() {
    const { id, content } = this.props.answer;
    return (
      <div onChange={this.handleChange} style={{ paddingBottom: 10 }}>
        <input
          value={id}
          type="radio"
          name={"answer" + this.props.item.id}
          title={content}
        ></input>
        <label>{content}</label>
      </div>
    );
  }
}

export default connect()(MultipleChoice);
