import { actionType } from "../actions/type";

const initialState = {
  questions: [],
  answers: {
    //questionId: formValue
  },
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionType.SET_QUESTION:
      state.questions = action.payload;
      return { ...state };
    case actionType.SET_ANSWER:
      state.answers = {
        ...state.answers,
        [action.payload.questionId]: action.payload,
      };
      return { ...state };
    default:
      return state;
  }
};
export default reducer;
